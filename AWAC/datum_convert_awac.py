
from datetime import datetime, timedelta

# with open("//Netapp1/MT/NT/MESSNETZ/OB2_current_awac.dat", "w") as fh_out:
with open("S:/Marnet.db/2023/DS/EMB318_DS_AWAC/DS_current_awac.dat", "w") as fh_out:
# with open("D:/Seereisen/EMB318/20230522_EMB318/DS_Daten/converted/DS_current_awac.dat", "w") as fh_out:
	### Datum holen und umwandeln
	# with open("S:/Marnet.db/2022/DS/EMB294_DS_AWAC_Data/DS211002.wap", "r") as fh_in_1:
	with open("D:/Seereisen/EMB318/20230522_EMB318/DS_Daten/converted/DS202208.wap", "r") as fh_in_1:
		datum = []
		v1 = []
		v1_temp = []
		v2 = []
		v2_temp = []
		line_nr = 0
		for line in fh_in_1:
			### Datumsformat anpassen (Dezimaltage zu DD.MM.YYYY HH:MM)
			line = line.split(" ")
			jahr = int(line[2])
			monat = int(line[0])
			tag = int(line[1])
			stunde = int(line[3])
			minute = 00
			# print(datum,monat,tag,jahr,stunde,minute)
			print("{:%Y%m%d%H%M} ".format(datetime(jahr,monat,tag,stunde,minute)))
			datum.append("{:%Y%m%d%H%M} ".format(datetime(jahr,monat,tag,stunde,minute)))
			line_nr = line_nr + 1
	### Ost-Komponente
	# with open("S:/Marnet.db/2022/DS/EMB294_DS_AWAC_Data/DS211002.v1", "r") as fh_in_2:
	with open("D:/Seereisen/EMB318/20230522_EMB318/DS_Daten/converted/DS202208.v1", "r") as fh_in_2:
		line_nr = 0
		i=0
		for line in fh_in_2:
			line = line.strip()
			line = line.replace("    ","   ")
			line = line.replace("   ","  ")
			line = line.replace("  "," ")
			line = line.split(" ")
			print(line_nr, line)
			print(len(line))
			# print(v1[line_nr])
			print(line[i])
			for i in range(0, len(line)):v1_temp.append(line[i])
			line_nr = line_nr + 1
			v1.append(v1_temp)
			v1_temp = []
	### Nord-Komponente
	# with open("S:/Marnet.db/2022/DS/EMB294_DS_AWAC_Data/DS211002.v2", "r") as fh_in_3:
	with open("D:/Seereisen/EMB318/20230522_EMB318/DS_Daten/converted/DS202208.v2", "r") as fh_in_3:
		line_nr = 0
		i=0
		for line in fh_in_3:
			line = line.strip()
			line = line.replace("    ","   ")
			line = line.replace("   ","  ")
			line = line.replace("  "," ")
			line = line.split(" ")
			
			for i in range(0, len(line)):v2_temp.append(line[i])
			print(len(line),line,v2_temp)
			line_nr = line_nr + 1
			v2.append(v2_temp)
			v2_temp = []
	### Format anpassen
	for i in range(0,len(datum)):
		insida_data = []
		print("{}".format(datum[i]),end='',file=fh_out)
		for j in range(0,len(v1[i])):
			len_test = v1[i][j].split(".")
			# print(len(len_test[0]))
			### Daten vorhanden oder leere Zelle?
			if len(len_test[0]) > 0:
				try:
					data = float(v1[i][j])
					### Parameter mit mehr als 4 Stellen vor dem Komma an die Formatierung anpassen (max. 8 Stellen)
					if len(len_test[0]) == 8:
						insida_data.append("{:8.0f}".format(data))
						# print("{:8.0f}".format(data))
					elif len(len_test[0]) == 7:
						insida_data.append("{:8.0f}".format(data))
						# print("{:8.0f}".format(data))
					elif len(len_test[0]) == 6:
						insida_data.append("{:8.1f}".format(data))
						# print("{:8.1f}".format(data))
					elif len(len_test[0]) == 5:
						insida_data.append("{:8.2f}".format(data))
						# print("{:8.2f}".format(data))
					else:
						# print("{:8.3f}".format(data))
						insida_data.append("{:8.3f}".format(data))
					# print("{}".format(insida_data[j]),end='',file=file_handler)
				except: 
					insida_data.append("{:8}".format(''))
			else:
				# print("leere Zelle")
				insida_data.append("{:8}".format(''))
				# print("{:8}".format(''),end='',file=file_handler)
			print("{}".format(insida_data[j]),end='',file=fh_out)
		for k in range(0,len(v2[i])):
			len_test = v2[i][k].split(".")
			# print(len(len_test[0]))
			### Daten vorhanden oder leere Zelle?
			if len(len_test[0]) > 0:
				try:
					data = float(v2[i][k])
					### Parameter mit mehr als 4 Stellen vor dem Komma an die Formatierung anpassen (max. 8 Stellen)
					if len(len_test[0]) == 8:
						insida_data.append("{:8.0f}".format(data))
						# print("{:8.0f}".format(data))
					elif len(len_test[0]) == 7:
						insida_data.append("{:8.0f}".format(data))
						# print("{:8.0f}".format(data))
					elif len(len_test[0]) == 6:
						insida_data.append("{:8.1f}".format(data))
						# print("{:8.1f}".format(data))
					elif len(len_test[0]) == 5:
						insida_data.append("{:8.2f}".format(data))
						# print("{:8.2f}".format(data))
					else:
						# print("{:8.3f}".format(data))
						insida_data.append("{:8.3f}".format(data))
					# print("{}".format(insida_data[j]),end='',file=file_handler)
				except: 
					insida_data.append("{:8}".format(''))
					print("error")
			else:
				print("leere Zelle")
				insida_data.append("{:8}".format(''))
				# print("{:8}".format(''),end='',file=file_handler)
			### j+1+k damit in der Liste auch die Daten der Nordkompente korrekt referenziert werden
			print("{}".format(insida_data[j+1+k]),end='',file=fh_out)
		print("",file=fh_out)