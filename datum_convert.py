
from datetime import datetime, timedelta

with open("S:/Marnet.db/2023/DS/20230926_MC_Data_DS/DS_12m.cnv", "w") as fh_out:
# with open("D:/Seereisen/EMB321/20230707_EMB321/OB_MC/OB_12m.cnv", "w") as fh_out:
	# print("DD.MM.YYYY HH:MM	U40P	U40T	U40S	U40C	U40D	U40Oxy	U40Oxsat",file=fh_out)
	# print("DD.MM.YYYY HH:MM	U43P	U43T	U43S	U43C	U43D	U43Oxsat",file=fh_out)
	# print("DD.MM.YYYY HH:MM	U25P	U25T	U25S	U25C	U25D	U25Oxsat",file=fh_out)
	# print("DD.MM.YYYY HH:MM	U16P	U16T	U16S	U16C	U16D	U16Oxsat",file=fh_out)
	# print("DD.MM.YYYY HH:MM	U07P	U07T	U07S	U07C	U07D	U07Oxy	U07Oxsat",file=fh_out)
	# print("DD.MM.YYYY HH:MM	U19P	U19T	U19S	U19C	U19D	U19Oxy	U19Oxsat",file=fh_out)
	# print("DD.MM.YYYY HH:MM	U03P	U03T	U03S	U03C	U03D	U03Oxy	U03Oxsat",file=fh_out)
	print("DD.MM.YYYY HH:MM	U12T	U12S	U12C	U12D	U12Oxsat",file=fh_out)
	# print("DD.MM.YYYY HH:MM	U12P	U12T	U12S	U12C	U12D	U12Oxy	U12Oxsat",file=fh_out)
	# print("DD.MM.YYYY HH:MM	U02P	U02T	U02S	U02C	U02D	U02Oxsat",file=fh_out)
	# print("DD.MM.YYYY HH:MM	U21P	U21T	U21S	U21C	U21D	U21Oxsat",file=fh_out)
	# print("DD.MM.YYYY HH:MM	U05T	U05S	U05C",file=fh_out)
	with open("S:/Marnet.db/2023/DS/20230926_MC_Data_DS/12_SBE37-IM_03707153_2023_09_26.cnv", "r") as fh_in:
	# with open("D:/Seereisen/EMB321/20230707_EMB321/OB_MC/12m_SBE37IMP-ODO_03724797_2023_07_10.cnv", "r") as fh_in:
		start = False
		datum = []
		temp = []
		leit = []
		salz = []
		press = []
		dens = []
		oxygen_ml = []
		oxsat_ml = []
		line_nr = 0
		start = False
		### start_time muss auf den 31.12. des Vorjahres gesetzt werden
		start_time = datetime(2021,12,31)
		for line in fh_in:
			if start == False:
				print(line.rstrip(), file=fh_out)
			if "*END*" in line: 
				start = True
				continue
			if start == True:
				### Datumsformat anpassen (Dezimaltage zu DD.MM.YYYY HH:MM)
				mc_time = float(line[0:11])
				mc_time_convert = start_time + timedelta(mc_time)
				jahr = mc_time_convert.strftime("%Y")
				monat = mc_time_convert.strftime("%m")
				tag = mc_time_convert.strftime("%d")
				stunde = mc_time_convert.strftime("%H")
				minute = mc_time_convert.strftime("%M")
				# sekunde = mc_time_convert.strftime("%S")
				sekunde = mc_time_convert.strftime("00")
				# print(mc_time_convert)
				# print("%s.%s.%s %s:%s" % (tag,monat,jahr,stunde,minute))
				# print(datum_fixed)
				####
				datum.append ("%s.%s.%s %s:%s:%s" % (tag,monat,jahr,stunde,minute,sekunde))
				print(datum[line_nr])
				try:
					temp.append(float(line[22:33]))
				except:
					temp.append("")
				try:
					dens.append(float(line[55:66]))
				except:
					dens.append("")
				try:
					leit.append(float(line[44:55]))
				except:
					leit.append("")
				try:
					press.append(float(line[11:22]))
				except:
					press.append("")
				try:
					salz.append(float(line[33:44]))
				except:
					salz.append("")
				# try:
					# oxygen_ml.append(float(line[66:77]))
				# except:
					# oxygen_ml.append("")
				try:
					oxsat_ml.append(float(line[66:77]))
				except:
					oxsat_ml.append("")
				try:
					leit[line_nr] = str(leit[line_nr]).replace(".",",")
					temp[line_nr] = str(temp[line_nr]).replace(".",",")
					press[line_nr] = str(press[line_nr]).replace(".",",")
					salz[line_nr] = str(salz[line_nr]).replace(".",",")
					# oxygen_ml[line_nr] = str(oxygen_ml[line_nr]).replace(".",",")
					oxsat_ml[line_nr] = str(oxsat_ml[line_nr]).replace(".",",")
					dens[line_nr] = str(dens[line_nr]).replace(".",",")
				except:
					print("leer")
				# print("%s	%s	%s	%s	%s	%s	%s" % (datum[line_nr],press[line_nr],temp[line_nr],salz[line_nr],leit[line_nr],dens[line_nr],oxsat_ml[line_nr]))
				# print("%s	%s	%s	%s	%s	%s	%s	%s" % (datum[line_nr],press[line_nr],temp[line_nr],salz[line_nr],leit[line_nr],dens[line_nr],oxygen_ml[line_nr],oxsat_ml[line_nr]))
				# print("%s	%s	%s	%s	%s	%s	%s" % (datum[line_nr],press[line_nr],temp[line_nr],salz[line_nr],leit[line_nr],oxygen_ml[line_nr],oxsat_ml[line_nr]))
				# print("%s	%s	%s	%s	%s" % (datum[line_nr],temp[line_nr],salz[line_nr],leit[line_nr],dens[line_nr]))
				print("%s	%s	%s	%s" % (datum[line_nr],temp[line_nr],leit[line_nr],salz[line_nr]))
				print(line_nr)
				line_nr = line_nr + 1
				
		for i in range(0,len(datum)):
			# print("%s	%s	%s	%s	%s	%s	%s" % (datum[i],press[i],temp[i],salz[i],leit[i],dens[i],oxsat_ml[i]),file=fh_out)
			# print("%s	%s	%s	%s	%s	%s	%s	%s" % (datum[i],press[i],temp[i],salz[i],leit[i],dens[i],oxygen_ml[i],oxsat_ml[i]),file=fh_out)
			# print("%s	%s	%s	%s	%s	%s	%s" % (datum[i],press[i],temp[i],leit[i],oxygen_ml[i],oxsat_ml[i],salz[i]),file=fh_out)
			# print("%s	%s	%s	%s	%s	%s" % (datum[i],press[i],temp[i],salz[i],leit[i],dens[i]),file=fh_out)
			# print("%s	%s	%s	%s	%s" % (datum[i],temp[i],salz[i],leit[i],dens[i]),file=fh_out)
			print("%s	%s	%s	%s" % (datum[i],temp[i],leit[i],salz[i]),file=fh_out)
			

# mc_time = 98.520961
