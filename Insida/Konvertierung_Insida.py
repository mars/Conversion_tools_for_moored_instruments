############
### anpassen der Meldungen der MARNET-Stationen auf Insida-MARNET83-Format
### Das Format f8.3 bedeutet: XXXX.YYY
### Für jeden Wert sind 4 Stellen vor dem Dezimaltrenner und 3 Stellen nach dem Dezimaltrenner vorgesehen. Also insgesamt 8 Stellen pro Messwert
### Es gibt zwischen den Werten keinen Trenner.
### Am Beginn der Zeile muss der Zeitstempel in folgendem Format stehen: YYYYMMDDhhmm
### Dann, ganz wichtig EIN LEERZEICHEN.
############
from datetime import datetime


def convert_marnet_data(datei_in,datei_out,datei_out2):
	with open(datei_out, "w") as fh_out:
		# with open(datei_out2, "w") as fh_out2:
		if datei_out2 != "":
			fh_out2 = open(datei_out2, "w")
		fh_in = open(datei_in, "r")
		if "Current" in datei_in:
			j_max = 52
			print("Current")
		else:
			j_max = 40
		for line in fh_in:
			file_handler = fh_out
			insida_data = []
			j = 0
			### Dezimaltrenner anpassen
			line = line.replace(",",".")
			###### Header Zeilen ausblenden
			if "DD.MM.YYYY HH:MM:SS" in line or "DD.MM.YYYY HH:MM" in line or "DD.MM.YY HH:MM" in line or "date" in line:
				continue
			else:
				line = line.split("\t")
				### Erkennung "Fake-Line" --> ganz viele leere Zellen, vorhandene Werte in der Zeile passen nicht zum Zeitpunkt
				# if line[1] == '' and line[2] == '' and line[3] == '' and line[4] == '' and line[5] == '' and line[6] == '':
					# continue
				### Parameter auftrennen, erst Datum anpassen
				datum = line[0]
				monat = int(datum[3:5])
				tag = int(datum[0:2])
				jahr = int(datum[6:10])
				stunde = int(datum[11:13])
				minute = int(datum[14:16])
				# print(datum,monat,tag,jahr,stunde,minute)
				print("{:%Y%m%d%H%M} ".format(datetime(jahr,monat,tag,stunde,minute)))
				insida_data.append("{:%Y%m%d%H%M} ".format(datetime(jahr,monat,tag,stunde,minute)))
				### Ausgabe ohne newline (end='')
				print("{}".format(insida_data[j]),end='',file=file_handler)
				j = j+1
				### alle Parameter vom Format anpassen
				for i in range(1,len(line)):
					# print(line[i])
					### Test auf Parameter MET_t2 --> wird nicht verwendet in DB, Beispiel: E2,434
					if 'E' in line[i]:
						# print('True')
						line[i] = ''
					len_test = line[i].split(".")
					# print(len(len_test[0]))
					### Daten vorhanden oder leere Zelle?
					if len(len_test[0]) > 0:
						try:
							data = float(line[i])
							### Parameter mit mehr als 4 Stellen vor dem Komma an die Formatierung anpassen (max. 8 Stellen)
							if len(len_test[0]) == 8:
								insida_data.append("{:8.0f}".format(data))
								# print("{:8.0f}".format(data))
							elif len(len_test[0]) == 7:
								insida_data.append("{:8.0f}".format(data))
								# print("{:8.0f}".format(data))
							elif len(len_test[0]) == 6:
								insida_data.append("{:8.1f}".format(data))
								# print("{:8.1f}".format(data))
							elif len(len_test[0]) == 5:
								insida_data.append("{:8.2f}".format(data))
								# print("{:8.2f}".format(data))
							elif len(len_test[0]) == 4:
								insida_data.append("{:8.3f}".format(data))
								# print("{:8.2f}".format(data))
							elif len(len_test[0]) > 8:
								### Reduzierung der Anzahl der Zeichen für den Fall dass Wert länger (beispielsweise Zeit beim UW-Modem)
								data_char=str(data)
								data=float(data_char[0:8])
								insida_data.append("{:8.0f}".format(data))
								# print("{:8.2f}".format(data))
							else:
								# print("{:8.3f}".format(data))
								insida_data.append("{:8.4f}".format(data))
							# print("{}".format(insida_data[j]),end='',file=file_handler)
						except: 
							insida_data.append("{:8}".format(''))
					else:
						# print("leere Zelle")
						insida_data.append("{:8}".format(''))
						# print("{:8}".format(''),end='',file=file_handler)
					print("{}".format(insida_data[j]),end='',file=file_handler)
					j = j+1
					### Maximum an Spalten für Insida erreicht?
					if j == j_max:
						### Datei für den ersten Teil abschließen
						print("",file=file_handler)
						file_handler = fh_out2
						print("{}".format(insida_data[0]),end='',file=file_handler)
				### neue Zeile beginnen
				print("",file=file_handler)
### Definition der Dateien
# Strömung
# datei_in_current = "S:/Marnet.db/Validierung/Sauerstoff/2017/AB_2017_oxy_val_1h.txt"
# datei_in_current = "D:/Seereisen/EMB321/20230707_EMB321/OB_MC/OB_12m.cnv"
datei_in_current = "S:/Marnet.db/2023/DS/EMB321_DS_MC/DS_19m.cnv"
# datei_out_current = "D:/Seereisen/EMB318/20230522_EMB318/DS_Daten/DS_21m_1h.dat"
# datei_out_current_2 = "D:/Seereisen/EMB318/20230522_EMB318/DS_Daten/OB_DATA_10min_2.dat"
datei_out_current = "S:/DS_19m_ODO.dat"
datei_out_current_2 = "S:/OB_DATA_1h_2.dat"
# try: convert_marnet_data(datei_in_current,datei_out_current,datei_out_current_2)
convert_marnet_data(datei_in_current,datei_out_current,datei_out_current_2)
# except: print("keine Strömungsdatei vorhanden!")
# Wellen
# datei_in_waves = "/insida/files_to_be_converted/AB_Waves.d1h"
# datei_out_waves = "/insida/import/IOW/inbox/AB_Waves.dat"
# try: convert_marnet_data(datei_in_waves,datei_out_waves,"")
# # convert_marnet_data(datei_in_waves,datei_out_waves,"")
# except: print("keine Wellendatei vorhanden!")
# # UW Status
# datei_in_uw_status = "/insida/files_to_be_converted/AB_UW_Status.d1h"
# datei_out_uw_status = "/insida/import/IOW/inbox/AB_UW_Status.dat"
# try: convert_marnet_data(datei_in_uw_status,datei_out_uw_status,"")
# except: print("keine UW_Statusdatei vorhanden!")
# # 10 Minuten Werte
# datei_in_10min = "/insida/files_to_be_converted/AB_DATA.d10"
# datei_out_10min = "/insida/import/IOW/inbox/AB_DATA_10min.dat"
# datei_out2_10min = "/insida/import/IOW/inbox/AB_DATA_10min_2.dat"
# try: convert_marnet_data(datei_in_10min,datei_out_10min,datei_out2_10min)
# except: print("keine 10 Minuten Datei vorhanden!")
# # 1h Werte
# datei_in_1h = "/insida/files_to_be_converted/AB_DATA.d1h"
# datei_out_1h = "/insida/import/IOW/inbox/AB_DATA_1h.dat"
# datei_out2_1h = "/insida/import/IOW/inbox/AB_DATA_1h_2.dat"
# try: convert_marnet_data(datei_in_1h,datei_out_1h,datei_out2_1h)
# except: print("keine Stundendatei vorhanden!")
# ### Darßer Schwelle
# # Strömung
# datei_in_current = "/insida/files_to_be_converted/DS_Current.d1h"
# datei_out_current = "/insida/import/IOW/inbox/DS_Current.dat"
# datei_out_current_2 = "/insida/import/IOW/inbox/DS_Current_2.dat"
# try: convert_marnet_data(datei_in_current,datei_out_current,datei_out_current_2)
# except: print("keine Strömungsdatei vorhanden!")
# # Wellen
# datei_in_waves = "/insida/files_to_be_converted/DS_Waves.d1h"
# datei_out_waves = "/insida/import/IOW/inbox/DS_Waves.dat"
# # try: convert_marnet_data(datei_in_waves,datei_out_waves,"")
# convert_marnet_data(datei_in_waves,datei_out_waves,"")
# # except: print("keine Wellendatei vorhanden!")
# # UW Status
# datei_in_uw_status = "/insida/files_to_be_converted/DS_UW_Status.d1h"
# datei_out_uw_status = "/insida/import/IOW/inbox/DS_UW_Status.dat"
# try: convert_marnet_data(datei_in_uw_status,datei_out_uw_status,"")
# except: print("keine UW_Statusdatei vorhanden!")
# # 10 Minuten Werte
# datei_in_10min = "/insida/files_to_be_converted/DS_DATA.d10"
# datei_out_10min = "/insida/import/IOW/inbox/DS_DATA_10min.dat"
# datei_out2_10min = "/insida/import/IOW/inbox/DS_DATA_10min_2.dat"
# try: convert_marnet_data(datei_in_10min,datei_out_10min,datei_out2_10min)
# except: print("keine 10 Minuten Datei vorhanden!")
# # 1h Werte
# datei_in_1h = "/insida/files_to_be_converted/DS_DATA.d1h"
# datei_out_1h = "/insida/import/IOW/inbox/DS_DATA_1h.dat"
# datei_out2_1h = "/insida/import/IOW/inbox/DS_DATA_1h_2.dat"
# try: convert_marnet_data(datei_in_1h,datei_out_1h,datei_out2_1h)
# except: print("keine Stundendatei vorhanden!")
# # Wettersensor WS3100 1h Werte
# datei_in_1h = "/data/insida-data/files_to_be_converted/DS_WS01.d1h"
# datei_out_1h = "/insida/import/IOW/inbox/DS_WS01_1h.dat"
# datei_out2_1h = "/insida/import/IOW/inbox/DS_WS01_1h_2.dat"
# try: convert_marnet_data(datei_in_1h,datei_out_1h,datei_out2_1h)
# except: print("keine Stundendatei vorhanden; WS3100!")
# # Wettersensor WS3100 10 Minuten Werte
# datei_in_1h = "/data/insida-data/files_to_be_converted/DS_WS01.d10"
# datei_out_1h = "/insida/import/IOW/inbox/DS_WS01_10min.dat"
# datei_out2_1h = "/insida/import/IOW/inbox/DS_WS01_10min_2.dat"
# try: convert_marnet_data(datei_in_1h,datei_out_1h,datei_out2_1h)
# except: print("keine 10 Minuten Datei vorhanden; WS3100!")
# ### Oder Bank
# # # Strömung
# # datei_in_current = "/insida/files_to_be_converted/DS_Current.d1h"
# # datei_out_current = "/insida/import/IOW/inbox/DS_Current.dat"
# # datei_out_current_2 = "/insida/import/IOW/inbox/DS_Current_2.dat"
# # try: convert_marnet_data(datei_in_current,datei_out_current,datei_out_current_2)
# # except: print("keine Strömungsdatei vorhanden!")
# # # Wellen
# # datei_in_waves = "/insida/files_to_be_converted/DS_Waves.d1h"
# # datei_out_waves = "/insida/import/IOW/inbox/DS_Waves.dat"
# # # try: convert_marnet_data(datei_in_waves,datei_out_waves,"")
# # convert_marnet_data(datei_in_waves,datei_out_waves,"")
# # # except: print("keine Wellendatei vorhanden!")
# # # UW Status
# # datei_in_uw_status = "/insida/files_to_be_converted/DS_UW_Status.d1h"
# # datei_out_uw_status = "/insida/import/IOW/inbox/DS_UW_Status.dat"
# # try: convert_marnet_data(datei_in_uw_status,datei_out_uw_status,"")
# # except: print("keine UW_Statusdatei vorhanden!")
# # 10 Minuten Werte
# datei_in_10min = "/insida/files_to_be_converted/OB_DATA.d10"
# datei_out_10min = "/insida/import/IOW/inbox/OB_DATA_10min.dat"
# datei_out2_10min = "/insida/import/IOW/inbox/OB_DATA_10min_2.dat"
# try: convert_marnet_data(datei_in_10min,datei_out_10min,datei_out2_10min)
# except: print("keine 10 Minuten Datei vorhanden!")
# # 1h Werte
# datei_in_1h = "/insida/files_to_be_converted/OB_DATA.d1h"
# datei_out_1h = "/insida/import/IOW/inbox/OB_DATA_1h.dat"
# datei_out2_1h = "/insida/import/IOW/inbox/OB_DATA_1h_2.dat"
# try: convert_marnet_data(datei_in_1h,datei_out_1h,datei_out2_1h)
# except: print("keine Stundendatei vorhanden!")