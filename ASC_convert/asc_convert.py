from tkinter import *
from tkinter import filedialog
# import seawater as gsw
import gsw

### Methode zur Auswahl der Datei
def open_file():
	name= filedialog.askopenfilename(filetypes=[("ASC Files","*.asc")])
	print(name)
	print(type(name))
	text_file.insert(0, name)
	return(name)
### Methode zum Speichern der Datei
def save_file(sn,upload_date):
	print(sn)
	print(upload_date)
	save_name= filedialog.asksaveasfilename(defaultextension='.d10', initialfile='xx_xm_%s_%s' % (sn,upload_date))
	print(save_name)
	return(save_name)
### Ausführen
def execute():
	druck = eingabe_tiefe.get()
	name = text_file.get()
	return(read_file(name,druck))
### Methode für Anpassung Monat
def month_correction(monat):
	if monat=="Jan" or monat=="jan": monat = "01"
	if monat=="Feb" or monat=="feb": monat = "02"
	if monat=="Mar" or monat=="mar": monat = "03"
	if monat=="Apr" or monat=="apr": monat = "04"
	if monat=="May" or monat=="may": monat = "05"
	if monat=="Jun" or monat=="jun" :monat = "06"
	if monat=="Jul" or monat=="jul": monat = "07"
	if monat=="Aug" or monat=="aug": monat = "08"
	if monat=="Sep" or monat=="sep": monat = "09"
	if monat=="Oct" or monat=="oct": monat = "10"
	if monat=="Nov" or monat=="nov": monat = "11"
	if monat=="Dec" or monat=="dec": monat = "12"
	return (monat)

### Methode zum Auslesen der Cal-Datei
def read_file(name,druck):
	line_counter = 0
	data = False
	header = False
	### Typen
	# 0 SBE37IM
	# 1 SBE37IMP
	# 2 SBE37IMP-ODO
	type = 0
	###
	fh_in = open(name, "r")
	T.insert(END, "Konvertierung der Datei --%s-- mit Druck %s dBar\n" %(name,druck))
	for line in fh_in:
		line_counter = line_counter + 1
#		print (line)
#		print (line_counter)
		if "* Temperature SN" in line:
			sn = line.split("=")
			sn = sn[1].replace(" ","")
			sn = sn.replace("\n","")
			print(sn)
		elif "* System UpLoad Time" in line:
			upload_date = line.split("=")
#			print(upload_date)
			upload_date = upload_date[1].split(" ")
#			print(upload_date)
			upload_date = "%s-%s-%s" % (upload_date[1],upload_date[2],upload_date[3])
			print(upload_date)
		elif "* pressure S/N" in line:
			### SBE37IMP --> mit Drucksensor
			type = 1
		elif "start sample number =" in line:
			data = True
			print(data)
			save_name = save_file(sn,upload_date)
			fh_out = open(save_name,"w")
		elif data == True:
			print(line)
			mc_daten = line.split(",")
			# prüfen in welcher Spalte das Datum steht
			if ":" in mc_daten[-1]:
				#### T,C,Dat;Uhr
				if "-" in mc_daten[-2]: 
					datum = mc_daten[-2].split("-")
					print(datum)
					mc_daten[-2] = "%s.%s.%s" % (datum[1],month_correction(datum[0]),datum[2])
				else: 
					datum = mc_daten[-2].split(" ")
					print(datum)
					mc_daten[-2] = "%s.%s.%s" % (datum[-3],month_correction(datum[-2]),datum[-1])
				
				for i in range(len(mc_daten)):
					mc_daten[i] = mc_daten[i].replace(" ","")
					mc_daten[i] = mc_daten[i].replace("\n","")
				### Uhrzeit korrigieren
				print(mc_daten[-1])
				# uhr = mc_daten[-1].split(":")
				# if "9" in uhr[-2]:
					# minute = str(int(uhr[-2])+1)
					# print(minute)
					# if "60" in minute:
						# minute = "00"
						# stunde = str(int(uhr[0])+1)
						# if len(stunde)==1:stunde="0%s"%stunde
						# if stunde == "24": stunde= "00"
					# else: stunde = uhr[0]
					# mc_daten[-1] = "%s:%s" %(stunde,minute)
					# print(mc_daten[-1])
				# if header == False:
					# print("DD.MM.YYYY HH:MM	Temperatur	Leitfähigkeit	Salz",file=fh_out)
					# header = True
				# # salt = gsw.eos80.salt((float(mc_daten[1])*10/42.194),float(mc_daten[0]),float(druck))
				# salt = gsw.SP_from_C(float(mc_daten[1])*10,float(mc_daten[0]),float(druck))
				# mc_daten[-1] = mc_daten[-1][0:5]
				# print("%s %s	%s	%s	%.4f" % (mc_daten[2],mc_daten[3],mc_daten[0],mc_daten[1],salt) ,file=fh_out)
				# # print("%s %s	%s	%s	%.4f" % (mc_daten[2],mc_daten[3],mc_daten[1],mc_daten[0],salt))
				if type == 0:
					if header == False:
						print("DD.MM.YYYY HH:MM	Temperatur	Leitfähigkeit	Salz",file=fh_out)
						header = True
					salt = gsw.SP_from_C(float(mc_daten[1])*10,float(mc_daten[0]),float(druck))
					# salt = gsw.eos80.salt((float(mc_daten[0])/42.194),float(mc_daten[1]),float(druck))
		#			salt = gsw.eos80.salt((float(mc_daten[0])/42.194),float(mc_daten[1]),2)
					mc_daten[3] = mc_daten[3][0:5]
					print("%s %s	%s	%s	%.4f" % (mc_daten[2],mc_daten[3],mc_daten[0],mc_daten[1],salt) ,file=fh_out)
					print("%s %s	%s	%s	%.4f" % (mc_daten[2],mc_daten[3],mc_daten[0],mc_daten[1],salt))
				elif type == 1:
					if header == False:
						print("DD.MM.YYYY HH:MM	Temperatur	Leitfähigkeit	Salz	Druck",file=fh_out)
						header = True
					salt = gsw.SP_from_C(float(mc_daten[1])*10,float(mc_daten[0]),float(mc_daten[2]))
					# salt = gsw.eos80.salt((float(mc_daten[0])/42.194),float(mc_daten[1]),float(mc_daten[2]))
					# mc_daten[-1] = mc_daten[-1][0:5]
					print("%s %s	%s	%s	%.4f	%s" % (mc_daten[-2],mc_daten[-1],mc_daten[0],float(mc_daten[1])*10,salt,mc_daten[2]) ,file=fh_out)
					print("%s %s	%s	%s	%.4f	%s" % (mc_daten[-2],mc_daten[-1],mc_daten[0],float(mc_daten[1])*10,salt,mc_daten[2]))
			elif "-" in mc_daten[-1]:
				#### C,T,Uhr,Dat
				datum = mc_daten[-1].split("-")
				mc_daten[-1] = "%s.%s.%s" % (datum[1],datum[0],datum[2])
				for i in range(len(mc_daten)):
					mc_daten[i] = mc_daten[i].replace(" ","")
					mc_daten[i] = mc_daten[i].replace("\n","")
				if type == 0:
					if header == False:
						print("DD.MM.YYYY HH:MM	Temperatur	Leitfähigkeit	Salz",file=fh_out)
						header = True
					salt = gsw.SP_from_C(float(mc_daten[0]),float(mc_daten[1]),float(druck))
					# salt = gsw.eos80.salt((float(mc_daten[0])/42.194),float(mc_daten[1]),float(druck))
		#			salt = gsw.eos80.salt((float(mc_daten[0])/42.194),float(mc_daten[1]),2)
					mc_daten[2] = mc_daten[2][0:5]
					print("%s %s	%s	%s	%.4f" % (mc_daten[3],mc_daten[2],mc_daten[1],mc_daten[0],salt) ,file=fh_out)
					print("%s %s	%s	%s	%.4f" % (mc_daten[3],mc_daten[2],mc_daten[1],mc_daten[0],salt))
				elif type == 1:
					if header == False:
						print("DD.MM.YYYY HH:MM	Temperatur	Leitfähigkeit	Salz	Druck",file=fh_out)
						header = True
					salt = gsw.SP_from_C(float(mc_daten[0]),float(mc_daten[1]),float(mc_daten[2]))
					# salt = gsw.eos80.salt((float(mc_daten[0])/42.194),float(mc_daten[1]),float(mc_daten[2]))
					mc_daten[-2] = mc_daten[-2][0:5]
					print("%s %s	%s	%s	%.4f	%s" % (mc_daten[-1],mc_daten[-2],mc_daten[1],mc_daten[0],salt,mc_daten[2]) ,file=fh_out)
					print("%s %s	%s	%s	%.4f	%s" % (mc_daten[-1],mc_daten[-2],mc_daten[1],mc_daten[0],salt,mc_daten[2]))
	T.insert(END, "Konvertierung beendet!")
	text_file.delete(0,END)
	eingabe_tiefe.delete(0,END)
errmsg = 'Error!'
master = Tk()
master.title("SBE37 MicroCat asc-to-d10 © IOW Robert Mars 2016")
#Button(text='.cal Datei auswählen', command=open_file).pack(fill=X)
Button(text='.asc Datei auswählen', command=open_file).pack()
text_file = Entry(master)
text_file.pack()
Label(master, text="Druck in dBar").pack()
eingabe_tiefe = Entry(master)
eingabe_tiefe.pack()
Button(text='Konvertierung ausführen', command=execute).pack()
T = Text(master, height=30, width=80)
T.pack()
mainloop()

