
from datetime import datetime, timedelta

# with open("//Netapp1/MT/NT/MESSNETZ/OB2_AWAC_Waves.dat", "w") as fh_out:
# with open("S:/Marnet.db/2022/DS/EMB294_DS_AWAC_Data/DS_AWAC_Waves.dat", "w") as fh_out:
with open("D:/Seereisen/EMB318/20230522_EMB318/DS_Daten/converted/DS_AWAC_Waves.dat", "w") as fh_out:
	### Datum holen und umwandeln
	# with open("S:/Marnet.db/2022/DS/EMB294_DS_AWAC_Data/DS211002.wap", "r") as fh_in_1:
	with open("D:/Seereisen/EMB318/20230522_EMB318/DS_Daten/converted/DS202208.wap", "r") as fh_in_1:
		datum = []
		welle = []
		welle_temp = []
		line_nr = 0
		for line in fh_in_1:
			line = line.strip()
			line = line.replace("    ","   ")
			line = line.replace("   ","  ")
			line = line.replace("  "," ")
			line = line.split(" ")
			jahr = int(line[2])
			monat = int(line[0])
			tag = int(line[1])
			stunde = int(line[3])
			minute = 00
			# print(datum,monat,tag,jahr,stunde,minute)
			print("{:%Y%m%d%H%M} ".format(datetime(jahr,monat,tag,stunde,minute)))
			datum.append("{:%Y%m%d%H%M} ".format(datetime(jahr,monat,tag,stunde,minute)))
			for i in range(6, len(line)):welle_temp.append(line[i])
			line_nr = line_nr + 1
			welle.append(welle_temp)
			welle_temp = []
	for i in range(0,len(datum)):
		insida_data = []
		print("{}".format(datum[i]),end='',file=fh_out)
		for j in range(0,len(welle[i])):
			len_test = welle[i][j].split(".")
			# print(len(len_test[0]))
			### Daten vorhanden oder leere Zelle?
			if len(len_test[0]) > 0:
				try:
					data = float(welle[i][j])
					### Parameter mit mehr als 4 Stellen vor dem Komma an die Formatierung anpassen (max. 8 Stellen)
					if len(len_test[0]) == 8:
						insida_data.append("{:8.0f}".format(data))
						# print("{:8.0f}".format(data))
					elif len(len_test[0]) == 7:
						insida_data.append("{:8.0f}".format(data))
						# print("{:8.0f}".format(data))
					elif len(len_test[0]) == 6:
						insida_data.append("{:8.1f}".format(data))
						# print("{:8.1f}".format(data))
					elif len(len_test[0]) == 5:
						insida_data.append("{:8.2f}".format(data))
						# print("{:8.2f}".format(data))
					else:
						# print("{:8.3f}".format(data))
						insida_data.append("{:8.3f}".format(data))
					# print("{}".format(insida_data[j]),end='',file=file_handler)
				except: 
					insida_data.append("{:8}".format(''))
			else:
				# print("leere Zelle")
				insida_data.append("{:8}".format(''))
				# print("{:8}".format(''),end='',file=file_handler)
			print("{}".format(insida_data[j]),end='',file=fh_out)
		print("",file=fh_out)